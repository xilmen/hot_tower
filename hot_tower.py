#!/usr/bin/env python3
import re

#Fonction grep
def grep(yourlist, yourstring):
    ide = [i for i, item in enumerate(yourlist) if re.search(yourstring, item)]
    return ide[0]

#Ouverture du fichier d'origine
f = open("tour_de_chauffe.gcode", "r", encoding="utf-8")
tab = f.readlines()
temps = list(range(180,211,5))

#Boucle sur la temperature et le hauteur de couche
for ligne in range(1,350,50):
    for temp in temps:
        index = grep(tab,"; layer "+str(ligne) + ", Z")
        print("Modification de la ligne {}".format(index))
        gcode = "M104 S" + str(temp) + '\n'
        tab.insert(index,gcode)
        print("Ajout de la température {}".format(temp))
        temps.remove(temp)
        break

o = open ("tour_de_chauffe_temp.gcode","w", encoding="utf-8")

#Boucle de création du nouveau fichier
for i in tab:
    o.write(i)

#Fermeture de tout les fichiers
f.close()
o.close()
