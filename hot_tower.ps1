#Browsing file
Add-Type -AssemblyName System.Windows.Forms
$FileBrowser = New-Object System.Windows.Forms.OpenFileDialog
$FileBrowser.filter = "gcode (*.gcode)| *.gcode"
[void]$FileBrowser.ShowDialog()
$FileBrowser.FileName


$gcode_file = $FileBrowser.FileName
$layer_list = @(1,50,100,150,200,250,300)
$temp_list = @(180,185,190,195,200,205,210)

foreach ($layer in $layer_list) {
    (Get-Content $gcode_file) | 
        Foreach-Object {
            $_ # send the current line to output
            if ($_ -match '^;LAYER:' + [regex]::escape($layer) + '$') 
            {
                #Add Lines after the selected pattern 
                "M104 S $($temp_list[$layer_list.IndexOf($layer)])"
            }
        } | Set-Content $gcode_file
}

[System.Windows.Forms.MessageBox]::Show("modification of the gcode is finished")